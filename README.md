# IPL STATS

This project displays HighCharts for five problems, i.e., Matches per season, Team wins per season,Extra runs conceded by teams in 2016, Economic Bowlers in 2015 and Highest wicket taker.

## Steps:
-- Loading data from csv file(matches, deliveries) to PostgreSQL database.
###
-- Writing query to obtain data from database according to the problem mentioned above (Django ORM).
###
-- Data visualization of for all five problems using HighCharts.
###
-- Creating APIs for matches and deliveries for GET, POST, PUT, DELETE request.
###
-- API includes all matches and per match details, all deliveries and per delivery details.

## Result for 5 problems:
![matches per season](./matches-per-season.png)
![team wins per season](./team-wins-per-season.png)
![extra runs conceded](./extra-runs-conceded-in-2.png)
![most economic bowlers](./most-economical-bowlers.png)
![highest wicket takers](./highest-wicket-takers.png)
