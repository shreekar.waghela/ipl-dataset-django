from django.db import models
from postgres_copy import CopyManager

# Create your models here.
class Match(models.Model):
    id = models.IntegerField(primary_key=True)
    season = models.IntegerField(null=True)
    city = models.CharField(max_length=150, null=True)
    date = models.DateTimeField(null=True)
    team1 = models.CharField(max_length=200, null=True)
    team2 = models.CharField(max_length=200, null=True)
    toss_winner = models.CharField(max_length=200, null=True)
    toss_decision = models.CharField(max_length=200, null=True)
    result = models.CharField(max_length=150, null=True)
    dl_applied = models.IntegerField(default=0, null=True)
    winner = models.CharField(max_length=200, null=True)
    win_by_runs = models.IntegerField(default=0, null=True)
    win_by_wickets = models.IntegerField(default=0, null=True)
    player_of_match = models.CharField(max_length=200, null=True)
    venue = models.CharField(max_length=300, null=True)
    umpire1 = models.CharField(max_length=200, null=True)
    umpire2 = models.CharField(max_length=200, null=True)
    umpire3 = models.CharField(max_length=200, null=True)
    objects = CopyManager()

    def __str__(self):
        return str(self.id)

class Delivery(models.Model):
    match_id = models.ForeignKey(Match, on_delete=models.CASCADE)
    inning = models.IntegerField(default=1, null=True)
    batting_team = models.CharField(max_length=200, null=True)
    bowling_team = models.CharField(max_length=200, null=True)
    over = models.IntegerField(default=1, null=True)
    ball = models.IntegerField(default=1, null=True)
    batsman = models.CharField(max_length=200, null=True)
    non_striker = models.CharField(max_length=200, null=True)
    bowler = models.CharField(max_length=200, null=True)
    is_super_over = models.IntegerField(default=0, null=True)
    wide_runs = models.IntegerField(default=0, null=True)
    bye_runs = models.IntegerField(default=0, null=True)
    legbye_runs = models.IntegerField(default=0, null=True)
    noball_runs = models.IntegerField(default=0, null=True)
    penalty_runs = models.IntegerField(default=0, null=True)
    batsman_runs = models.IntegerField(default=0, null=True)
    extra_runs = models.IntegerField(default=0, null=True)
    total_runs = models.IntegerField(default=0, null=True)
    player_dismissed = models.CharField(max_length=200, null=True)
    dismissal_kind = models.CharField(max_length=150,null=True)
    fielder = models.CharField(max_length=200, null=True)
    objects = CopyManager()

    def __str__(self):
        return str(self.id)
