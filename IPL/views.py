from django.shortcuts import render, get_object_or_404
from IPL.models import Match, Delivery
from django.http import HttpResponse, JsonResponse
from django.db.models import Count, Sum, When, Case, F, FloatField
from django.db.models.functions import Cast

#Imports for cache
from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
#Imports for api
from django.views.decorators.csrf import csrf_exempt
import json
from .serializers import MatchSerializer, DeliverySerializer
from rest_framework import generics

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

# Create your views here.


def index(request):
    return render(request, 'IPL/index.html')

# Matches Per Season


def api_matches_per_season(request):
    matches_per_season = {}

    details = Match.objects.values('season').annotate(
        matches=Count('season')).order_by('season')

    for detail in details:
        matches_per_season[detail['season']] = detail['matches']
    return JsonResponse(matches_per_season)

@cache_page(CACHE_TTL)
def matches_per_season(request):
    return render(request, 'IPL/matches_per_season.html')

# Team wins per season


def api_team_wins_per_season(request):
    team_wins_per_season = {}

    details = Match.objects.values('season', 'winner').annotate(
        wins=Count('winner')).exclude(winner=None).order_by('season', 'winner')
    
    season_list = []
    for detail in details:
        if detail['season'] not in season_list:
            season_list.append(detail['season'])
        team = detail['winner']
        season = detail['season']
        wins = detail['wins']
        if team in team_wins_per_season:
           if season in team_wins_per_season[team]:
               team_wins_per_season[team][season] += wins
           else:
               team_wins_per_season[team][season] = wins
        else:
           team_wins_per_season[team] = {season : wins}
    for team in team_wins_per_season:
        for year in season_list:
            if year not in team_wins_per_season[team]:
                team_wins_per_season[team][year] = 0    
    return JsonResponse(team_wins_per_season)

@cache_page(CACHE_TTL)
def team_wins_per_season(request):
    return render(request, 'IPL/team_wins_per_season.html')

# Extra runs conceded


def api_extra_runs_conceded(request):
    extra_runs_conceded = {}

    details = Delivery.objects.values('bowling_team').filter(
        match_id__season=2016, is_super_over=0).annotate(extras=Sum('extra_runs'))

    for detail in details:
        extra_runs_conceded[detail['bowling_team']] = detail['extras']

    return JsonResponse(extra_runs_conceded)

@cache_page(CACHE_TTL)
def extra_runs_conceded(request):
    return render(request, 'IPL/extra_runs_conceded.html')

# Economic Bowlers


def api_economic_bowlers(request):
    economic_bowlers = {}

    details = Delivery.objects.filter(match_id__season=2015, is_super_over=False).values('bowler').annotate(runs=Sum('batsman_runs')+Sum('wide_runs')+Sum('noball_runs')).annotate(balls=Count(
        'ball')-Count(Case(When(noball_runs__gt=0, then=1)))-Count(Case(When(wide_runs__gt=0, then=1)))).annotate(economy=Cast((F('runs')/(F('balls')/6.0)), FloatField())).order_by('economy')[:10]

    for detail in details:
        economic_bowlers[detail['bowler']] = detail['economy']

    return JsonResponse(economic_bowlers)

@cache_page(CACHE_TTL)
def economic_bowlers(request):
    return render(request, 'IPL/economic_bowlers.html')

# Highest wicket takers


def api_highest_wicket_takers(request):
    highest_wicket_taker = {}

    details = Delivery.objects.values('bowler').exclude(dismissal_kind='run out').annotate(
        wickets=Count('player_dismissed')).order_by('-wickets')[:10]

    for detail in details:
        highest_wicket_taker[detail['bowler']] = detail['wickets']

    return JsonResponse(highest_wicket_taker)


def highest_wicket_takers(request):
    return render(request, 'IPL/highest_wicket_takers.html')

####################################################
#API Implementation for GET, POST, PUT, DELETE
####################################################
#Matches GET
@csrf_exempt
def api_matches(request):
    if request.method == 'GET':
        matches = Match.objects.all()
    return JsonResponse(list(matches.values()), safe=False)

#Match details GET
@csrf_exempt
def api_match_details(request, pk):
    if request.method == 'GET':
        match_details = Match.objects.values().filter(id = pk)
        return JsonResponse(list(match_details),safe=False)
    elif request.method == 'DELETE':
        detail = Match.objects.filter(id=pk)
        detail.delete()
        return HttpResponse('Record Deleted!!')
    elif request.method == 'POST':
        detail = request.body.decode(encoding='utf-8')
        detail = json.loads(detail)

        id = len(Match.objects.all())+1
        season = detail['season']
        city = detail['city']
        date = detail['date']
        team1 = detail['team1']
        team2 = detail['team2']
        toss_winner = detail['toss_winner']
        toss_decision = detail['toss_decision']
        result = detail['result']
        dl_applied = detail['dl_applied']
        winner = detail['winner']
        win_by_runs = detail['win_by_runs']
        win_by_wickets = detail['win_by_wickets']
        player_of_match = detail['player_of_match']
        venue = detail['venue']
        umpire1 = detail['umpire1']
        umpire2 = detail['umpire2']
        umpire3 = detail['umpire3']

        obj = Match(id = id, season = season, city = city, date = date, team1 = team1, team2 = team2, toss_winner = toss_winner, toss_decision = toss_decision, result = result, dl_applied = dl_applied, winner = winner, win_by_runs = win_by_runs, win_by_wickets = win_by_wickets, player_of_match  = player_of_match, venue = venue,  umpire1 = umpire1, umpire2 = umpire2, umpire3 = umpire3 )
        obj.save()
        return HttpResponse('Match Detail Created!!')  
    elif request.method == "PUT":
        detail = request.body.decode(encoding='utf-8')
        detail = json.loads(detail)

        season = detail['season']
        city = detail['city']
        date = detail['date']
        team1 = detail['team1']
        team2 = detail['team2']
        toss_winner = detail['toss_winner']
        toss_decision = detail['toss_decision']
        result = detail['result']
        dl_applied = detail['dl_applied']
        winner = detail['winner']
        win_by_runs = detail['win_by_runs']
        win_by_wickets = detail['win_by_wickets']
        player_of_match = detail['player_of_match']
        venue = detail['venue']
        umpire1 = detail['umpire1']
        umpire2 = detail['umpire2']
        umpire3 = detail['umpire3']
        
        obj = Match.objects.filter(id=pk).update(season = season, city = city, date = date, team1 = team1, team2 = team2, toss_winner = toss_winner, toss_decision = toss_decision, result = result, dl_applied = dl_applied, winner = winner, win_by_runs = win_by_runs, win_by_wickets = win_by_wickets, player_of_match  = player_of_match, venue = venue,  umpire1 = umpire1, umpire2 = umpire2, umpire3 = umpire3)
        return HttpResponse('Updated Detail')


#Deliveries GET
@csrf_exempt
def api_deliveries(request):
    if request.method == 'GET':
        deliveries = Delivery.objects.all()
    return JsonResponse(list(deliveries.values()), safe=False)

#Delivery Details GET
@csrf_exempt
def api_delivery_details(request, pk):
    if request.method == 'GET':
        delivery_details = Delivery.objects.values().filter(id=pk)
        return JsonResponse(list(delivery_details.values()), safe=False)
    elif request.method == 'DELETE':
        detail = Delivery.objects.filter(id=pk)
        detail.delete()
        return HttpResponse('Record Deleted!!')
    elif request.method == 'POST':
        detail = request.body.decode(encoding='utf-8')
        detail = json.loads(detail)

        id = len(Delivery.objects.all())+1
        inning = detail['inning']
        batting_team = detail['batting_team']
        bowling_team = detail['bowling_team']
        over = detail['over']
        ball = detail['ball']
        batsman = detail['batsman']
        non_striker = detail['non_striker']
        bowler = detail['bowler']
        is_super_over = detail['is_super_over']
        wide_runs = detail['wide_runs']
        bye_runs = detail['bye_runs']
        legbye_runs = detail['legbye_runs']
        noball_runs = detail['noball_runs']
        penalty_runs = detail['penalty_runs']
        batsman_runs = detail['batsman_runs']
        extra_runs = detail['extra_runs']
        total_runs = detail['total_runs']
        player_dismissed = detail['player_dismissed']
        dismissal_kind = detail['dismissal_kind']
        fielder = detail['fielder']

        obj = Delivery(id=id, inning=inning, batting_team=batting_team, bowling_team=bowling_team, over=over, ball=ball, batsman=batsman, non_striker=non_striker, bowler=bowler, is_super_over=is_super_over, wide_runs=wide_runs, bye_runs=bye_runs, legbye_runs=legbye_runs, noball_runs=noball_runs, penalty_runs=penalty_runs, batsman_runs=batsman_runs, extra_runs=extra_runs, total_runs=total_runs, player_dismissed=player_dismissed, dismissal_kind=dismissal_kind, fielder=fielder)
        obj.save()
        return HttpResponse('Delivery Data Created')
    elif request.method == 'PUT':
        detail = request.body.decode(encoding='utf-8')
        detail = json.loads(detail)

        inning = detail['inning']
        batting_team = detail['batting_team']
        bowling_team = detail['bowling_team']
        over = detail['over']
        ball = detail['ball']
        batsman = detail['batsman']
        non_striker = detail['non_striker']
        bowler = detail['bowler']
        is_super_over = detail['is_super_over']
        wide_runs = detail['wide_runs']
        bye_runs = detail['bye_runs']
        legbye_runs = detail['legbye_runs']
        noball_runs = detail['noball_runs']
        penalty_runs = detail['penalty_runs']
        batsman_runs = detail['batsman_runs']
        extra_runs = detail['extra_runs']
        total_runs = detail['total_runs']
        player_dismissed = detail['player_dismissed']
        dismissal_kind = detail['dismissal_kind']
        fielder = detail['fielder']

        obj = Delivery.objects.filter(id=pk).update(inning=inning, batting_team=batting_team, bowling_team=bowling_team, over=over, ball=ball, batsman=batsman, non_striker=non_striker, bowler=bowler, is_super_over=is_super_over, wide_runs=wide_runs, bye_runs=bye_runs, legbye_runs=legbye_runs, noball_runs=noball_runs, penalty_runs=penalty_runs, batsman_runs=batsman_runs, extra_runs=extra_runs, total_runs=total_runs, player_dismissed=player_dismissed, dismissal_kind=dismissal_kind, fielder=fielder)
        return HttpResponse('Updated data!!')

# class MatchList(generics.ListCreateAPIView):
#     queryset = Match.objects.all()
#     serializer_class = MatchSerializer

# class MatchDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Match.objects.all()
#     serializer_class = MatchSerializer

# class DeliveryList(generics.ListCreateAPIView):
#     queryset = Delivery.objects.all()
#     serializer_class = DeliverySerializer

# class DeliveryDetail(generics.RetrieveUpdateDestroyAPIView):
#     queryset = Delivery.objects.all()
#     serializer_class = DeliverySerializer