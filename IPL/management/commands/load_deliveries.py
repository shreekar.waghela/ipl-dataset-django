import csv
from django.db import transaction
from IPL.models import Delivery
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        insert_deliveries = Delivery.objects.from_csv('/home/user/MountBlue_projects/django_ipl_data_project/ipl_dataset/IPL/management/commands/deliveries.csv')
        print("{} records inserted.".format(insert_deliveries))