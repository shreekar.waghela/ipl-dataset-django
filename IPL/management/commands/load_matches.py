import csv
from django.db import transaction
from IPL.models import Match
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        insert_match = Match.objects.from_csv('/home/user/MountBlue_projects/django_ipl_data_project/ipl_dataset/IPL/management/commands/matches.csv')
        print("{} records inserted.".format(insert_match))