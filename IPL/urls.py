from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('api/api_matches_per_season', views.api_matches_per_season, name= 'api_matches_per_season'),
    path('matches_per_season', views.matches_per_season, name='matches_per_season'),
    path('api/api_team_wins_per_season', views.api_team_wins_per_season, name='api_team_wins_per_season'),
    path('team_wins_per_season', views.team_wins_per_season, name='team_wins_per_season'),
    path('api/api_extra_runs_conceded', views.api_extra_runs_conceded, name='api_extra_runs_conceded'),
    path('extra_runs_conceded', views.extra_runs_conceded, name='extra_runs_conceded'),
    path('api/api_highest_wicket_takers', views.api_highest_wicket_takers, name='api_highest_wicket_takers'),
    path('highest_wicket_takers', views.highest_wicket_takers, name='highest_wicket_takers'),
    path('api/api_economic_bowlers', views.api_economic_bowlers, name='api_economic_bowlers'),
    path('economic_bowlers', views.economic_bowlers, name='economic_bowlers'),
    path('matches', views.api_matches, name='api_matches'),
    path('matches/<int:pk>/', views.api_match_details, name='api_match_details'),
    path('deliveries', views.api_deliveries, name='api_deliveries'),
    path('deliveries/<int:pk>/', views.api_delivery_details, name='api_delivery_details'),
    # path('matches',views.MatchList.as_view()),
    # path('matches/<int:pk>/',views.MatchDetail.as_view()),
    # path('deliveries',views.DeliveryList.as_view()),
    # path('deliveries/<int:pk>/',views.DeliveryDetail.as_view()),
]